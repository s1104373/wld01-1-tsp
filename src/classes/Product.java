package classes;
/**
 *
 * @author Briyan Kleijn
 */
public class Product {
    private int artikelNr;
    private int volume;
    
    public int getVolume(){
        return this.volume;
    }
    
    public int getArtikelNr(){
        return this.artikelNr;
    }
}
